import Button from './Components/Button'
import { useState } from 'react'

const App = () => {

  const [roll1, setRoll1] = useState('0')
  const [roll2, setRoll2] = useState('0')

  const rolling = () => {
    let roller1 = Math.floor((Math.random() * 6) + 1)
    let roller2 = Math.floor((Math.random() * 6) + 1)
    setRoll1(roller1)
    setRoll2(roller2)
  }


  return (
    <div className="container">
        <h1>{roll1}&nbsp;&nbsp;{roll2}</h1>
        <h3>Total: {roll1 + roll2}</h3>
        <Button color='green' text='Roll!' onClick={rolling}></Button>
    </div>
  )
}

export default App;
